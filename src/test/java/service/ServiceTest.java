package service;

import domain.Grade;
import domain.Homework;
import domain.Student;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.junit.platform.commons.util.ExceptionUtils;
import repository.GradeXMLRepository;
import repository.HomeworkXMLRepository;
import repository.StudentXMLRepository;
import validation.GradeValidator;
import validation.HomeworkValidator;
import validation.StudentValidator;
import validation.Validator;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

class ServiceTest {

    public static Service service;
    public static Service serviceBefore;

    @org.junit.jupiter.api.BeforeAll
    public static void setUp() {
        Validator<Student> studentValidator = new StudentValidator();
        Validator<Homework> homeworkValidator = new HomeworkValidator();
        Validator<Grade> gradeValidator = new GradeValidator();

        StudentXMLRepository fileRepository1 = new StudentXMLRepository(studentValidator, "students.xml");
        HomeworkXMLRepository fileRepository2 = new HomeworkXMLRepository(homeworkValidator, "homework.xml");
        GradeXMLRepository fileRepository3 = new GradeXMLRepository(gradeValidator, "grades.xml");

        service = new Service(fileRepository1, fileRepository2, fileRepository3);
    }

    @org.junit.jupiter.api.AfterEach
    void tearDown() {
    }

    @org.junit.jupiter.api.Test
    void findAllStudents() {
    }

    @org.junit.jupiter.api.Test
    void findAllHomework() {
    }

    @org.junit.jupiter.api.Test
    void findAllGrades() {
    }

    @org.junit.jupiter.api.Test
    void saveStudent() {
    }

    // @Test
    // @DisplayName("add valid homework")
    // void saveValidHomework() {
    //     Homework homework = new Homework("9", "verVal",12, 1);
    //     int result = serviceBefore.saveHomework(homework.getID(), homework.getDescription(), homework.getDeadline(), homework.getStartline());
    //     serviceBefore.deleteHomework(homework.getID());
    //     Assertions.assertEquals(0, result);
    // }

    @Test
    @DisplayName("add invalid student")
    void saveInValidStudent() {
        Student s = new Student("90", "Jhon",1000);
        int result = serviceBefore.saveStudent(s.getID(), s.getName(),s.getGroup());
        serviceBefore.deleteStudent(s.getID());
        Assertions.assertEquals(1, result);
    }

    @Test
    @DisplayName("add valid student")
    void saveValidStudent() {
        Student s = new Student("9", "Jhon",532);
        int result = serviceBefore.saveStudent(s.getID(), s.getName(),s.getGroup());
        serviceBefore.deleteStudent("9");
        Assertions.assertEquals(0, 0);

    }
    
    @Test
    @DisplayName("add invalid homework")
    void saveInValidHomework() {
        Homework homework = new Homework("", "Jhon",19, 1);
        int result = serviceBefore.saveHomework(homework.getID(), homework.getDescription(), homework.getDeadline(), homework.getStartline());
        serviceBefore.deleteHomework(homework.getID());
        Assertions.assertEquals(1, result);
    }

    @org.junit.jupiter.api.Test
    @DisplayName("checking if homework save works")
    void saveValidHomework() {
        Homework hw = new Homework("77", "some easy homework", 6, 2);
        int result = service.saveHomework(hw.getID(), hw.getDescription(), hw.getDeadline(), hw.getStartline());
        assertEquals(0, result);
        //assertTrue(result == 1);
        service.deleteHomework(hw.getID());
    }

    @org.junit.jupiter.api.Test
    void saveGrade() {
    }

    @Test
    void deleteStudent() {
        Student student = new Student(null, "Maria", 222);
        int result = serviceBefore.deleteStudent(student.getID());
        Assertions.assertTrue(result == 1);
        serviceBefore.saveStudent(student.getID(),student.getName(),student.getGroup());

    }

    @Test
    void deleteHomework() {
        Homework homework = new Homework("1", "File", 7,6);
        int result = serviceBefore.deleteHomework(homework.getID());
        Assertions.assertTrue(result == 1);
        serviceBefore.saveHomework(homework.getID(), homework.getDescription(), homework.getDeadline(), homework.getStartline());
    }


    @Test
    void updateHomework2() {
        Homework homework = new Homework("1", "File",7, 6);
        String newDescription = "newFile";
        int newDeadline = 8;
        int newStartDate = 4;
        if(newDescription instanceof String){
            int result = serviceBefore.updateHomework(homework.getID(), newDescription, newDeadline, newStartDate);
            Assertions.assertAll("newValues",
                    ()->Assertions.assertEquals(newDescription, serviceBefore.findOneHomework(homework.getID()).getDescription()),
                    ()->Assertions.assertEquals(newDeadline, serviceBefore.findOneHomework(homework.getID()).getDeadline()),
                    ()->Assertions.assertEquals(newStartDate, serviceBefore.findOneHomework(homework.getID()).getStartline()),
                    ()->Assertions.assertEquals(0,result)
            );
            serviceBefore.updateHomework(homework.getID(),homework.getDescription(),homework.getDeadline(), homework.getStartline());
        } else{
            System.out.println("nem megfelelo tipus a leiras");
        }

        
    }

    @Test
    void updateHomework1() {
        Homework homework = new Homework("1", "File",7, 6);
        String newDescription = "newFile";
        int newDeadline = 8;
        int newStartDate = 4;
        int result = serviceBefore.updateHomework(homework.getID(), newDescription, newDeadline, newStartDate);
        Assertions.assertAll("newValues",
                ()->Assertions.assertEquals(newDescription, serviceBefore.findOneHomework(homework.getID()).getDescription()),
                ()->Assertions.assertEquals(newDeadline, serviceBefore.findOneHomework(homework.getID()).getDeadline()),
                ()->Assertions.assertEquals(newStartDate, serviceBefore.findOneHomework(homework.getID()).getStartline()),
                ()->Assertions.assertEquals(0,result)
        );
        serviceBefore.updateHomework(homework.getID(),homework.getDescription(),homework.getDeadline(), homework.getStartline());

    }

    @org.junit.jupiter.api.Test
    void updateStudent() {
    }

    @org.junit.jupiter.api.Test
    void updateHomework() {
    }

    @org.junit.jupiter.api.Test
    void extendDeadline() {
    }

    @org.junit.jupiter.api.Test
    void createStudentFile() {
    }

//    @Test
//    void assertAllTest(){
//        Student s = new Student("99", "Johan", 533);
//        assertAll(
//                "student",
//                () -> assertEquals("99", s.getID()),
//                () -> assertEquals("Johan", s.getName())
//        );
//    }
//
//    @ParameterizedTest
//    @ValueSource(ints = {-10, 55, 533})
//    void testStudentAddByGroup(int group){
//        assumeTrue(group >= 0);
//        Student s = new Student("99", "Johan", group);
//        int result = service.saveStudent(s.getID(), s.getName(), s.getGroup());
//        assertEquals(1, result);
//        service.deleteStudent(s.getID());
//    }


}